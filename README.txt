Random Passcode Text Widget
================================

This module provides a ready made text field widget
with a generate button.
While clicking the button an alphanumeric code
is auto generated & set as the value of the field.
In the field instance setting admin can select
the no. of digits that the alphanumeric code
should contain.

Installation
============

Place this module at <drupal root>/modules/contrib/
and then install it.
In order to be able to create a field,
add or edit any entity type & add one field
of this type & chnage the setting in the manage
form display section if needed.
